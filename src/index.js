import React from "react";
import ReactDOM from "react-dom/client";
import Shape from "./page/Shape";
import Stopwatch from "./page/Stopwatch";
import "./index.css";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <div className="content">
      <Stopwatch />
      <Shape />
    </div>
  </React.StrictMode>
);
