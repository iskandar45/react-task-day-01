Stopwatch & Shape
=========

Komponen stopwatch sederhana dan Shape yang dibangun dengan React.

Penggunaan
----------

Untuk menggunakan komponen ini dalam aplikasi React Anda, ikuti langkah-langkah berikut:

1.  Salin file `Stopwatch.js` dan `Shape.js` ke dalam proyek Anda.
2.  Impor komponen ke dalam file Anda: `import Stopwatch from './Stopwatch';`
3.  Impor komponen ke dalam file Anda: `import Shape from './Shape';`
4.  Gunakan komponen `Stopwatch` dalam JSX Anda: `<Stopwatch />`
5.  Gunakan komponen `Shape` dalam JSX Anda: `<Shape />`

Props
-----

Komponen ini tidak menerima props apapun.

Preview
-------
![Preview Stopwatch](./docs/Stopwatch.png)
![Preview Shape](./docs/shape.png)

Cara Kerja
----------
## Stopwatch
Komponen `Stopwatch` menggunakan hook `useState` untuk mengelola state `time` dan state `isRunning`. State `time` adalah bilangan yang mewakili jumlah detik yang telah berlalu sejak stopwatch mulai. State `isRunning` adalah boolean yang menunjukkan apakah stopwatch sedang berjalan atau tidak.

Komponen juga menggunakan hook `useEffect` untuk memulai dan menghentikan timer yang memperbarui state `time` setiap detik selama stopwatch berjalan.

Fungsi `formatTime` digunakan untuk memformat state `time` menjadi string yang menampilkan waktu yang telah berlalu dalam jam, menit, dan detik.

Fungsi `handleStart`, `handleStop`, dan `handleReset` digunakan untuk mengendalikan stopwatch. `handleStart` mengatur state `isRunning` menjadi `true`, yang memulai timer. `handleStop` mengatur state `isRunning` menjadi `false`, yang menghentikan timer. `handleReset` mengatur state `time` menjadi `0` dan state `isRunning` menjadi `false`, yang mengatur ulang stopwatch.

## Shape
Tidak memiliki alur kerja, hanya berisi sebuah bentuk/Shape yang di bangun dengan CSS.

Styling
-------

Stopwatch termasuk file CSS sederhana (`Stopwatch.css`) yang menyediakan beberapa styling dasar.
Shape merupakan file CSS utama (`Shape.css`) yang berisi code CSS dari bentuk Shape.