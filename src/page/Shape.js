import React from "react";
import "../assets/css/Shape.css";

const Shape = () => {
  return (
    <div className="container">
      <h1 className="title">Shape</h1>
      <div className="wrapper">
        <div className="rak">
          <div className=" rak1">
            <div className="box-orangered"></div>
            <div className="box-deeppink"></div>
          </div>
          <div className=" rak2">
            <div className="triangle-orange"></div>
            <div className="ring-bluesky"></div>
          </div>
          <div className=" rak3">
            <div className="block-green"></div>
          </div>
        </div>
        <div className="rak">
          <div className="block-lime"></div>
        </div>
      </div>
    </div>
  );
};

export default Shape;
