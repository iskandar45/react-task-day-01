import React, { useState, useEffect } from "react";
import "../assets/css/Stopwatch.css";

const Stopwatch = () => {
  const [time, setTime] = useState(0);
  const [isRunning, setIsRunning] = useState(false);

  useEffect(() => {
    let interval;
    if (isRunning) {
      interval = setInterval(() => {
        setTime((prevTime) => prevTime + 1);
      }, 1000);
    }
    return () => clearInterval(interval);
  }, [isRunning]);

  const handleStart = () => {
    setIsRunning(true);
  };

  const handleStop = () => {
    setIsRunning(false);
  };

  const handleReset = () => {
    setTime(0);
    setIsRunning(false);
  };

  const formatTime = (timeInSeconds) => {
    const hours = Math.floor(timeInSeconds / 3600);
    // Membuat angka menjadi dua digit
    // .toString().padStart(2, "0");
    const minutes = Math.floor((timeInSeconds % 3600) / 60);
    // .toString().padStart(2, "0");
    const seconds = timeInSeconds % 60;
    // .toString().padStart(2, "0");
    return `${hours} : ${minutes} : ${seconds}`;
  };

  return (
    <div className="container">
      <h1 className="title">Stopwatch</h1>
      <div className="wrapper">
        <div className="timer">
          <p className="number">{formatTime(time)}</p>
          <p className="timer-text">Jam : Menit : Detik</p>
        </div>
        <div className="tombol">
          <button className="btn btn-reset" onClick={handleReset}>
            Reset
          </button>
          <button className="btn btn-start" onClick={handleStart}>
            Start
          </button>
          <button className="btn btn-stop" onClick={handleStop}>
            Stop
          </button>
        </div>
      </div>
    </div>
  );
};

export default Stopwatch;
